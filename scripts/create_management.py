from datetime import date
import argparse
import json
import os

def main():
    parser = argparse.ArgumentParser(description = "Create a new project")
    parser.add_argument("--project-path", nargs = 1, default = os.getcwd(), help = "Set the main project path to use this manager")
    parser.add_argument("--project-name", nargs = 1, help = "Set the project's name")
    parser.add_argument("--date-in", help = "Project creation date")

    args = parser.parse_args()

    project_path = args.project_path
    filename = ".project-task-manager.json"

    print("project-path:", project_path)

    if (args.date_in == None):
        while (True):
            try:
                date_in_str = input("date-in (YYYY-MM-DD): ")
                date_in = date.fromisoformat(date_in_str)

                break

            except ValueError:
                print ("Please, set the correct format of date...")
    else:
        try:
            print(args.date_in)
            date_in = date.fromisoformat(args.date_in)

        except ValueError:
            print ("Please, set the correct format of date...")
            quit()

    if (args.project_name == None):
        while (True):
            project_name = input("project-name: ")

            if (project_name):
                break
    else:
        if (not args.project_name):
            print ("Set a valid project name")
            quit()

    print("date-in", date_in)
    print("project-name", project_name)
    print("\n")
    print("Creating...", filename)

    project_filepath = os.path.join(project_path, filename)

    try:
        with open(project_filepath, "xt", encoding = "utf-8") as fp:
            data = {
                "project-name": project_name,
                "date-in": date_in.isoformat()
            }

            json.dump(data, fp, indent = 4, ensure_ascii = True)

    except FileExistsError:
        print ("The file \"" + project_filepath + "\" exists.")
        quit()

    folders_to_create = []

    project_data_folder = os.path.join(project_path, "data")

    folders_to_create.append(project_data_folder)
    folders_to_create.append(os.path.join(project_data_folder, "user"))
    folders_to_create.append(os.path.join(project_data_folder, "interrupt"))
    folders_to_create.append(os.path.join(project_data_folder, "performed"))

    for folder in folders_to_create:
        if (os.path.exists(folder)):
            print("The path \"" + folder + "\" exists")
            quit()

    for folder in folders_to_create:
        os.makedirs(folder)
        print("Create \"" + folder + "\"")



if __name__ == '__main__':
    main()
