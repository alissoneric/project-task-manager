from datetime import date
import argparse
import json
import os

from type.user import User


def treat_username(username):
    pass

def main():
    parser = argparse.ArgumentParser(description = "Create and add new user to project")
    parser.add_argument("--project-path", nargs = 1, default = os.getcwd(), help = "Set the main project path to use this manager")
    parser.add_argument("--date-in", help = "User date creation")
    parser.add_argument("--username", help = "Name to indentify the new user")

    args = parser.parse_args()

    project_path = args.project_path

    if (args.username == None):
        while (True):
            username = input("username: ")
            if (username):
                break
    else:
        username = args.username

    if (args.date_in == None):
        while (True):
            date_now = date.today()
            date_in_str = input("date-in (" + date_now.isoformat() + "): ")
            if (date_in_str):
                try:
                    date_in = date.fromisoformat(date_in_str)
                    break
                except ValueError:
                    print ("Please, set the correct format of date...")
            else:
                date_in = date_now
                break
    else:
        try:
            print(args.date_in)
            date_in = date.fromisoformat(args.date_in)

        except ValueError:
            print ("Please, set the correct format of date...")
            quit(1)


    try:
        user = User(username, date_in, None)
        user.save(project_path)
        print("The user '" + username + "' has been created")

    except Exception as e:
        print(e)

if __name__ == '__main__':
    main()
