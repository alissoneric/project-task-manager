from enum import Enum
from datetime import date
from datetime import time
from datetime import datetime
import json

class AttributeType(Enum):
    NUMBER = 1
    STRING = 2
    DATE = 3
    TIME = 4
    DATETIME = 5
    BOOL = 6

class Attribute:

    type_to_str = {
        AttributeType.NUMBER : "number",
        AttributeType.STRING : "string",
        AttributeType.DATE : "date",
        AttributeType.TIME : "time",
        AttributeType.DATETIME : "datetime",
        AttributeType.BOOL : "boolean"
    }

    str_to_type = {v: k for k, v in type_to_str.items()}

    def __init__(self, key = "", attr_type = AttributeType.STRING, required = True):
        self.key = key
        self.attr_type = attr_type
        self.required = required

    def load(self, dictionary):
        self.key = dictionary["key"]
        self.attr_type = str_to_type[dictionary["type"]]
        self.required = dictionary["required"]

    def to_dict(self):
        key = self.key
        required = self.required

        return {
            "key" : key,
            "type" : Attribute.type_to_str[self.attr_type],
            "required" : required
        }

    def convert_value(self, input_string_value):
        if (self.attr_type == AttributeType.NUMBER):
            return int(input_string_value)

        elif (self.attr_type == AttributeType.STRING):
            return str(input_string_value)

        elif (self.attr_type == AttributeType.DATE):
            return date.fromisoformat(input_string_value).isoformat()

        elif (self.attr_type == AttributeType.TIME):
            return time.fromisoformat(input_string_value).isoformat()

        elif (self.attr_type == AttributeType.DATETIME):
            return datetime.fromisoformat(input_string_value).isoformat()

        elif (self.attr_type == AttributeType.BOOL):
            if (input_string_value.lower() in ("yes", "true", "t", "1")):
                return True
            elif (input_string_value.lower() in ("no", "false", "f", "0")):
                return False
            else:
                raise ValueError("Invalid boolean value")

    def to_json(self):
        return json.dumps(self.to_dict(), indent = 4, ensure_ascii = True)
