from datetime import date
import json
import os
import io
import uuid
import glob

class User:
    def __init__(self):
        self.name = ""
        self.date_in = None
        self.date_out = None


    def __init__(self, name = "", date_in = None, date_out = None):
        self.name = name
        self.date_in = date_in
        self.date_out = date_out


    def load(self, project_path, name):
        filepath = os.path.join(project_path, "data", "user", name)
        filepath_info = os.path.join(filepath, "info.json")

        if (not os.path.exists(filepath)):
            raise Exception("The '" + name + "' user doesn't exists.")

        try:
            with io.open(filepath_info, "r") as fp:
                json_object = json.load(fp)
                self.name = json_object["username"]
                self.date_in = date.fromisoformat(json_object["date-in"])
                if "date-out" in json_object:
                    self.date_out = date.fromisoformat(json_object["date-out"])
                else:
                    self.date_out = None
        except (FileExistsError, ValueError):
            raise


    def save(self, project_path):
        filepath = os.path.join(project_path, "data", "user", self.name)

        if (not os.path.exists(filepath)):
            os.makedirs(filepath)
            os.makedirs(os.path.join(filepath, "to-perform"))

        filepath = os.path.join(filepath, "info.json")

        with io.open(filepath, "wt", encoding = "utf-8") as fp:
            data = {
                "username": self.name,
                "date-in": self.date_in.isoformat()
            }

            if (self.date_out != None):
                data["date-out"] = self.date_out.isoformat()

            json.dump(data, fp, indent = 4, ensure_ascii = True)


    def add_to_perform_interrupt(self, project_path, interrupt, attribute_values, start_datetime, end_datetime = None):
        interrupt_id = uuid.uuid4().hex

        filepath = os.path.join(project_path, "data", "user", self.name, "to-perform", interrupt_id + ".json")

        try:
            with io.open(filepath, "xt", encoding = "utf-8") as fp:
                data = {
                    "type": interrupt.name,
                    "start-datetime": start_datetime,
                    "attributes": attribute_values
                }

                if (end_datetime):
                    data["end-datetime"] = end_datetime

                json.dump(data, fp, indent = 4, ensure_ascii = True)
        except FileExistsError:
            raise Exception("File '" + filepath + "' already exists.")


    def get_all_to_perform(self, project_path):
        to_perform_folder = os.path.join(project_path, "data", "user", self.name, "to-perform")

        filepaths = glob.glob(to_perform_folder + "/*.json")
        tasks = []

        for filepath in filepaths:
            try:
                with io.open(filepath, "r") as fp:
                    obj = json.load(fp)
                    obj["filename"] = os.path.basename(filepath)
                    tasks.append(obj)

            except FileExistsError:
                raise Exception("File '" + filepath + "' already exists.")

        return tasks

    def close_task(self, project_path, to_perform_name, end_datetime):
        filepath = os.path.join(project_path, "data", "user", self.name, "to-perform", to_perform_name)

        try:
            with io.open(filepath, "r") as fp:
                obj = json.load(fp)
                obj["end-datetime"] = end_datetime
        except FileExistsError:
            raise Exception("File '" + filepath + "' not found.")

        with io.open(filepath, "wt", encoding = "utf-8") as fp:
            json.dump(obj, fp, indent = 4, ensure_ascii = True)
