import os
import io
import json

class Interrupt:

    def __init__(self, name = "", description = "", attributes = []):
        self.name = name
        self.description = description
        self.attributes = attributes

    def load(self, project_path, name):
        filepath = os.path.join(project_path, "data", "interrupt", name + ".json")

        try:
            with io.open(filepath, "r") as fp:
                json_object = json.load(fp)
                self.name = json_object["name"]
                self.description = json_object["description"]
                self.attributes = json_object["attributes"]

        except FileExistsError:
            raise Exception("The '" + name + "' interrupt type doesn't exists.")

    def save(self, project_path):
        filepath = os.path.join(project_path, "data", "interrupt", self.name + ".json")

        try:
            with io.open(filepath, "xt", encoding = "utf-8") as fp:
                data = {
                    "name" : self.name,
                    "description" : self.description,
                    "attributes" : self.attributes
                }

                json.dump(data, fp, indent = 4, ensure_ascii = True)

        except FileExistsError:
            raise Exception("The '" + self.name + "' interrupt type already exists.")
