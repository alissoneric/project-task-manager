from datetime import date
import argparse
import json
import os

from type.attribute import Attribute
from type.interrupt import Interrupt

def read_attibute():
    print()

    key = input("key: ")

    while (True):
        required_str = input("required (true or false): ")

        if (required_str.lower() == "true"):
            required = True
            break
        elif (required_str.lower() == "false"):
            required = False
            break
        else:
            print("invalid value")

    while (True):
        value_type = input("type (h - help): ")

        if (value_type == "h" or value_type == "help"):
            print("values for type:")
            print("\tnumber")
            print("\tstring")
            print("\tdate")
            print("\ttime")
            print("\tdatetime")
            print("\tboolean")
            print()
        elif not (value_type in Attribute.str_to_type):
            print("invalid type")
            continue
        else:
            break

    return Attribute(key, Attribute.str_to_type[value_type], required).to_dict()

def main():
    parser = argparse.ArgumentParser(description = "Create a new interrupt type to the project")
    parser.add_argument("--project-path", nargs = 1, default = os.getcwd(), help = "Set the main project path to use this manager")
    parser.add_argument("--name", nargs = 1, help = "Name of the interrupt type")
    parser.add_argument("--description", nargs = 1, help = "Description of the interrupt")

    args = parser.parse_args()

    project_path = args.project_path

    if (args.name == None):
        while (True):
            name = input("name: ")
            if (name):
                break
    else:
        name = args.name

    filepath = os.path.join(project_path, "data", "interrupt", name + ".json")
    if (os.path.exists(filepath)):
        print("The '" + name + "' interrupt type already exists")
        quit(1)

    if (args.description == None):
        while (True):
            description = input("description: ")
            if (description):
                break
    else:
        description = args.description

    attributes = []
    while (True):
        print()
        print("1 - finish")
        print("2 - add attribute")
        print("3 - cancel")

        option = int(input("> "))

        if (option == 1):
            break

        elif (option == 2):
            attributes.append(read_attibute())

        elif (option == 3):
            quit()

    try:
        interrupt = Interrupt(name, description, attributes)
        interrupt.save(project_path)
    except Exception as e:
        print(e)
        quit()


if __name__ == '__main__':
    main()
