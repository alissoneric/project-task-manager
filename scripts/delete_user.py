from datetime import date
import argparse
import json
import os

from type.user import User


def main():
    parser = argparse.ArgumentParser(description = "Remove user from project")
    parser.add_argument("--project-path", nargs = 1, default = os.getcwd(), help = "Set the main project path to use this manager")
    parser.add_argument("--date-out", help = "Project creation date")
    parser.add_argument("--username", help = "Name to indentify the new user")

    args = parser.parse_args()

    project_path = args.project_path

    if (args.username == None):
        while (True):
            username = input("username: ")
            if (username):
                break
    else:
        username = args.username

    try:
        user = User()
        user.load(project_path, username)

    except Exception as e:
        print(e)
        quit(1)

    if (args.date_out == None):
        while (True):
            try:
                date_out_str = input("date-out (YYYY-MM-DD): ")
                date_out = date.fromisoformat(date_out_str)
                break

            except ValueError:
                print ("Please, set the correct format of date...")
    else:
        try:
            print(args.date_out)
            date_out = date.fromisoformat(args.date_out)

        except ValueError:
            print ("Please, set the correct format of date...")
            quit()

    print(type(date_out))

    try:
        user.date_out = date_out
        user.save(project_path)
        print("The '" + username + "' user has been deleted")

    except Exception as e:
        print(e)

if __name__ == '__main__':
    main()
