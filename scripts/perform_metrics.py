import argparse
import json
import os
import io

from datetime import date
from type.user import User

def main():
    parser = argparse.ArgumentParser(description = "Add a new user interrupt to the project")
    parser.add_argument("--project-path", nargs = 1, default = os.getcwd(), help = "Set the main project path to use this manager")
    parser.add_argument("--metrics-date", help = "Creation date of metrics")

    args = parser.parse_args()

    project_path = args.project_path

    if (args.metrics_date == None):
        while (True):
            date_now = date.today()
            metrics_date_str = input("date of metrics (" + date_now.isoformat() + "): ")
            if (metrics_date_str):
                try:
                    metrics_date = date.fromisoformat(metrics_date_str)
                    break
                except ValueError:
                    print ("Please, set the correct format of date...")
            else:
                metrics_date = date_now
                break
    else:
        try:
            print(args.metrics_date)
            metrics_date = date.fromisoformat(args.metrics_data)

        except ValueError:
            print ("Please, set the correct format of date...")
            quit(1)

    project_user_path = os.path.join(project_path, "data", "user")
    user_filepaths = filter(os.path.isdir, [os.path.join(project_user_path, dirname) for dirname in os.listdir(project_user_path)])
    usernames = [os.path.basename(name) for name in user_filepaths]

    metrics = {}
    files_to_remove = []
    for username in usernames:
        try:
            user = User()
            user.load(project_path, username)

            to_perform = user.get_all_to_perform(project_path)
            metrics[username] = to_perform

        except Exception as e:
            print(e)
            quit(1)

    try:
        with io.open(os.path.join(project_path, "data", "performed", metrics_date.isoformat() + "-metrics.json"), "xt") as fp:
            json.dump(metrics, fp, indent = 4, ensure_ascii = True)

    except Exception as e:
        print(e)
        quit(1)

    for username in usernames:
        for interrupt in metrics[username]:
            filepath = os.path.join(project_path, "data", "user", username, "to-perform", interrupt["filename"])

            try:
                os.remove(filepath)
            except Exception as e:
                print(e)


if __name__ == '__main__':
    main()
