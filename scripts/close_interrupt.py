import argparse
import json
import os

from datetime import datetime
from type.user import User

def main():
    parser = argparse.ArgumentParser(description = "Close an interrupt of user")
    parser.add_argument("--project-path", nargs = 1, default = os.getcwd(), help = "Set the main project path to use this manager")
    parser.add_argument("--user", nargs = 1, help = "User associated to the interrupt to create")
    parser.add_argument("--date-out", nargs = 1, help = "End datetime of interruption")

    args = parser.parse_args()

    project_path = args.project_path

    if (args.user == None):
        while (True):
            username = input("user: ")
            if (username):
                break
    else:
        username = args.user

    try:
        user = User()
        user.load(project_path, username)
    except Exception as e:
        print(e)
        quit(1)

    try:
        all_interrupts = user.get_all_to_perform(project_path)
        interrupts = []

        for interrupt in all_interrupts:
            if not ("end-datetime" in interrupt):
                interrupts.append(interrupt)

    except Exception as e:
        print(e)
        quit(1)

    if len(interrupts) == 0:
        print ("Nothing to do")
        return

    for index in range(0, len(interrupts)):
        interrupt = interrupts[index]
        print ("[", index + 1, "]", interrupt["type"])

        for attribute in interrupt["attributes"]:
            print ("\t", attribute, ":", interrupt["attributes"][attribute])

        print()

    while (True):
        try:
            index_to_close = int(input("> "))
            index_to_close -= 1

            if (index_to_close >= 0 and index_to_close < len(interrupts)):
                break

            print("Invalid value")

        except ValueError:
            print("Invalid value")

    if (args.date_out == None):
        while (True):
            datetime_now = datetime.now()
            datetime_str = input("end datetime (" + datetime_now.isoformat() + "): ")
            if (datetime_str):
                try:
                    end_datetime = datetime.fromisoformat(datetime_str).isoformat()
                    break
                except ValueError:
                    print ("Please, set the correct format of datetime...")
            else:
                end_datetime = datetime_now.isoformat()
                break
    else:
        try:
            end_datetime = datetime.fromisoformat(args.date_out).isoformat()
        except ValueError:
            print ("Please, set the correct format of datetime...")
            quit(1)

    user.close_task(project_path, interrupts[index_to_close]["filename"], end_datetime)


if __name__ == '__main__':
    main()
