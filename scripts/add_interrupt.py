from datetime import date, datetime
import argparse
import json
import os

from type.user import User
from type.interrupt import Interrupt
from type.attribute import Attribute

def read_attribute(attribute):
    name = attribute["key"]

    while (True):
        value = input(name + ": ");
        if (value or not attribute["required"]):
            break

    return name, value

def main():
    parser = argparse.ArgumentParser(description = "Add a new user interrupt to the project")
    parser.add_argument("--project-path", nargs = 1, default = os.getcwd(), help = "Set the main project path to use this manager")
    parser.add_argument("--type", nargs = 1, help = "Type of interruption")
    parser.add_argument("--user", nargs = 1, help = "User associated to the interrupt to create")

    args = parser.parse_args()

    project_path = args.project_path

    if (args.user == None):
        while (True):
            interrupt_user = input("user: ")
            if (interrupt_user):
                break
    else:
        interrupt_user = args.user

    try:
        user = User()
        user.load(project_path, interrupt_user)
    except Exception as e:
        print(e)
        quit(1)

    if (args.type == None):
        while (True):
            interrupt_type = input("type: ")
            if (interrupt_type):
                break
    else:
        interrupt_type = args.type


    try:
        interrupt = Interrupt()
        interrupt.load(project_path, interrupt_type)
    except Exception as e:
        print(e)
        quit(1)

    while (True):
        datetime_now = datetime.now()
        datetime_str = input("start datetime (" + datetime_now.isoformat() + "): ")
        if (datetime_str):
            try:
                start_datetime = datetime.fromisoformat(datetime_str).isoformat()
                break
            except ValueError:
                print ("Please, set the correct format of datetime...")
        else:
            start_datetime = datetime_now.isoformat()
            break

    while (True):
        datetime_str = input("end datetime: ")
        if (datetime_str):
            try:
                end_datetime = datetime.fromisoformat(datetime_str).isoformat()
                break
            except ValueError:
                print ("Please, set the correct format of datetime...")
        else:
            end_datetime = ""
            break

    attribute_values = {}
    for attribute in interrupt.attributes:
        name, value = read_attribute(attribute)

        if (value):
            attribute_type = Attribute(attribute["key"], Attribute.str_to_type[attribute["type"]], attribute["required"])
            try:
                attribute_values[name] = attribute_type.convert_value(value)
            except Exception as e:
                print (e)
                quit(1)

    try:
        user.add_to_perform_interrupt(project_path, interrupt, attribute_values, start_datetime, end_datetime)
    except Exception as e:
        print (e)
        quit(1)

if __name__ == '__main__':
    main()
